django\_couch.sitemaps package
==============================

Submodules
----------

django\_couch.sitemaps.views module
-----------------------------------

.. automodule:: django_couch.sitemaps.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_couch.sitemaps
   :members:
   :undoc-members:
   :show-inheritance:
