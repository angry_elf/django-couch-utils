django\_couch.flatpages package
===============================

Submodules
----------

django\_couch.flatpages.admin module
------------------------------------

.. automodule:: django_couch.flatpages.admin
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.flatpages.forms module
------------------------------------

.. automodule:: django_couch.flatpages.forms
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.flatpages.middleware module
-----------------------------------------

.. automodule:: django_couch.flatpages.middleware
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.flatpages.sitemaps module
---------------------------------------

.. automodule:: django_couch.flatpages.sitemaps
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_couch.flatpages
   :members:
   :undoc-members:
   :show-inheritance:
