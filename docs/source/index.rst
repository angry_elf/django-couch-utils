.. django-couch-utils documentation master file, created by
   sphinx-quickstart on Wed May  3 12:31:35 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-couch-utils's documentation!
==============================================

Contents:

Installation
============
::

  pip install django-couch-utils


Configuration
=============


::
   
  INSTALLED_APPS = [
      ...
      'django_couch',
  ]

  ...

  MIDDLEWARE = [
      ...
      'django_couch.CouchMiddleware',
  ]
  
  COUCHDB_VIEWS_DIR = BASE_DIR /  'couchdb-design-docs'  # where couchdb views stores
  COUCHDB_SERVER = 'http://username:password@172.17.0.1:5984'
  COUCHDB = {
      'db': {
          'server': COUCHDB_SERVER,
          'database': 'example',
          'default' : True,
      },
      'db_other': {
          'server': COUCHDB_SERVER,
          'database': 'example_other',
      },
  }


Usage
=====

After configuring, you can use couchdb directly from your views:

::

   def index(request):
       doc = request.db[document_id]
       doc.field = 'new'
       doc.save()



Stored views
============

To store couchdb view files on disk, please follow storage schema below:
Create directory per database record inside COUCHDB_VIEWS_DIR, so you may create this files:

::
   
   couchdb-design-docs/
   couchdb-design-docs/db
   couchdb-design-docs/db/projects.js  # .js extension is important here - it marks view as javascript code
   couchdb-design-docs/db/projects.js/views
   couchdb-design-docs/db/projects.js/views/list
   couchdb-design-docs/db/projects.js/views/active
   couchdb-design-docs/db/projects.js/views/active/map.js # .js extension here is for syntax highlight in your code editor only


Now run
::
   ./manage.py couch restore -n db -f
   
This example will create view named ``projects/list`` inside db. ``-f`` flag means force operations. Default is to dry run without any changes

       
Hint
----

Default django behaviour is to store sessions in database. To eliminate this activity, use another storage engine, for example, file storage or cookie storage::

  settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'

  or

  SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
  SESSION_COOKIE_HTTPONLY = True


.. toctree::
   :maxdepth: 2
   :caption: Contents:


API
===

.. toctree::
   :maxdepth: 2

   helpers
   authentication
   flatpages
   sitemaps


Manage.py extensions
====================

.. toctree::
   :maxdepth: 2

   managepy


   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
