django\_couch.admin package
===========================

Submodules
----------

django\_couch.admin.deprecated module
-------------------------------------

.. automodule:: django_couch.admin.deprecated
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.admin.forms module
--------------------------------

.. automodule:: django_couch.admin.forms
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.admin.urls module
-------------------------------

.. automodule:: django_couch.admin.urls
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.admin.views module
--------------------------------

.. automodule:: django_couch.admin.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_couch.admin
   :members:
   :undoc-members:
   :show-inheritance:
