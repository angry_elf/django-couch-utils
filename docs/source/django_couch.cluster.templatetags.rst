django\_couch.cluster.templatetags package
==========================================

Submodules
----------

django\_couch.cluster.templatetags.cluster\_extras module
---------------------------------------------------------

.. automodule:: django_couch.cluster.templatetags.cluster_extras
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_couch.cluster.templatetags
   :members:
   :undoc-members:
   :show-inheritance:
