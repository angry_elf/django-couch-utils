django\_couch package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_couch.admin
   django_couch.auth
   django_couch.cluster
   django_couch.flatpages
   django_couch.management
   django_couch.sitemaps

Submodules
----------

django\_couch.context\_processors module
----------------------------------------

.. automodule:: django_couch.context_processors
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.generic module
----------------------------

.. automodule:: django_couch.generic
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.shortcuts module
------------------------------

.. automodule:: django_couch.shortcuts
   :members:
   :undoc-members:
   :show-inheritance:

django\_couch.translit module
-----------------------------

.. automodule:: django_couch.translit
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: django_couch
   :members:
   :undoc-members:
   :show-inheritance:
